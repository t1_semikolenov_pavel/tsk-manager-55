package ru.t1.semikolenov.tm.command.user;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.semikolenov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.semikolenov.tm.api.endpoint.IUserEndpoint;
import ru.t1.semikolenov.tm.command.AbstractCommand;
import ru.t1.semikolenov.tm.dto.model.UserDTO;

@Getter
@Component
public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    protected void showUser(final UserDTO user) {
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}