package ru.t1.semikolenov.tm.command.system;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.semikolenov.tm.api.service.ICommandService;
import ru.t1.semikolenov.tm.api.service.IPropertyService;
import ru.t1.semikolenov.tm.command.AbstractCommand;
import ru.t1.semikolenov.tm.enumerated.Role;

@Getter
@Component
public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private ICommandService commandService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}