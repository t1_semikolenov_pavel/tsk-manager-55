package ru.t1.semikolenov.tm.command.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.semikolenov.tm.api.endpoint.IProjectTaskEndpoint;
import ru.t1.semikolenov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.semikolenov.tm.command.AbstractCommand;
import ru.t1.semikolenov.tm.enumerated.Role;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.dto.model.TaskDTO;
import ru.t1.semikolenov.tm.util.DateUtil;

import java.util.List;

@Getter
@Component
public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IProjectTaskEndpoint projectTaskEndpoint;

    protected void renderTasks(@NotNull final List<TaskDTO> tasks) {
        int index = 1;
        for (TaskDTO task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void showTask(@NotNull final TaskDTO task) {
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(task.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(task.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(task.getDateEnd()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}