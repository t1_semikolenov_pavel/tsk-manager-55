package ru.t1.semikolenov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.semikolenov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.semikolenov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.semikolenov.tm.api.service.IPropertyService;
import ru.t1.semikolenov.tm.dto.request.*;
import ru.t1.semikolenov.tm.dto.response.*;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.marker.ISoapCategory;
import ru.t1.semikolenov.tm.dto.model.ProjectDTO;
import ru.t1.semikolenov.tm.service.PropertyService;
import ru.t1.semikolenov.tm.util.DateUtil;

import java.util.Date;

@Category(ISoapCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String host = propertyService.getServerHost();

    @NotNull
    private final String port = Integer.toString(propertyService.getServerPort());

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(host, port);

    @Nullable
    private String token;

    @Nullable
    private ProjectDTO projectBefore;

    @Before
    public void init() {
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("test", "test"));
        token = loginResponse.getToken();
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        @NotNull final ProjectCreateResponse createResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(token, "test_one", "test_one", null, null)
        );
        projectBefore = createResponse.getProject();
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(null, projectBefore.getId(), Status.IN_PROGRESS)));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(token, projectBefore.getId(), null)));
        ProjectChangeStatusByIdResponse response = projectEndpoint.changeProjectStatusById(
                new ProjectChangeStatusByIdRequest(token, projectBefore.getId(), Status.IN_PROGRESS));
        Assert.assertNotNull(response);
        @Nullable ProjectDTO project = response.getProject();
        Assert.assertNotEquals(projectBefore.getStatus(), project.getStatus());
    }

    @Test
    public void createProject() {
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.createProject(
                        new ProjectCreateRequest(null, "", "", null, null)));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.createProject(
                        new ProjectCreateRequest(token, "", "", null, null)));
        @NotNull final String projectName = "test_name";
        @NotNull final String projectDescription = "test_description";
        @NotNull final Date dateBegin = DateUtil.toDate("01.01.2020");
        @NotNull final Date dateEnd = DateUtil.toDate("01.01.2021");
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(token, projectName, projectDescription, dateBegin, dateEnd));
        Assert.assertNotNull(response);
        @Nullable ProjectDTO project = response.getProject();
        Assert.assertEquals(projectName, project.getName());
    }

    @Test
    public void removeProjectByIdTest() {
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(null, projectBefore.getId())));
        projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(token, projectBefore.getId()));
        Assert.assertNull(projectEndpoint.listProject(new ProjectListRequest(token, null)).getProjects());
    }

    @Test
    public void showProjectById() {
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.showProjectById(new ProjectShowByIdRequest(null, projectBefore.getId())));
        @NotNull final ProjectShowByIdResponse response = projectEndpoint.showProjectById(
                new ProjectShowByIdRequest(token, projectBefore.getId()));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getProject());
    }

    @Test
    public void updateProjectById() {
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest(null, projectBefore.getId(), "", "")));
        @NotNull final ProjectUpdateByIdResponse response = projectEndpoint.updateProjectById(
                new ProjectUpdateByIdRequest(token, projectBefore.getId(), "new_name", "new_description"));
        Assert.assertNotNull(response);
        @Nullable ProjectDTO project = response.getProject();
        Assert.assertNotNull(project);
        Assert.assertNotEquals(projectBefore.getName(), project.getName());
    }

}
