package ru.t1.semikolenov.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.api.repository.dto.IUserDtoOwnedRepository;
import ru.t1.semikolenov.tm.dto.model.AbstractUserDtoOwnedModel;
import ru.t1.semikolenov.tm.enumerated.Status;

import java.util.Date;

public interface IUserDtoOwnedService<M extends AbstractUserDtoOwnedModel> extends IUserDtoOwnedRepository<M>, IDtoService<M> {

    @Nullable
    M create(@Nullable String userId, @Nullable String name);

    @Nullable
    M create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    M create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    @Nullable
    M updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    M changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

}
